FROM perl:5.34-slim

RUN cpanm  Net::DNS \
&& cpanm Net::IP \
&& cpanm IO::Socket::INET6 -force \
&& cpanm Net::Address::IP::Local -force
